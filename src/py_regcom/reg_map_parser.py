
class RegisterDescriptor:
    """Read-Only register descriptor

    Details:
        This class is meant as read-only description of a single
        register item in the register map.
    """

    def __init__(self, addr, name, factor = 1, writable = False):
        #! Hardware register address of this register
        self.address = addr

        #! Readable name of this register
        self.name = name

        #! Conversion factor between hardware-units
        #! and real units
        self.conversion_factor = factor

        #! Whether or not this register is writable
        self.writable = writable

    def __getitem__(self, key):
        return self.__dict__[key]

class RegMapParser:
    """RegMap Parser class

    Details:
        This class is intended as parser for the standard RegisterMap
        Excel and/or CSV files.

        Its main purpose is to provide an easy read-only interface 
        to the register map, and provide common conversion and 
        lookup functions.
    """

    def __init__(self, register_file):

        #! Main register map.
        #!
        #! @details This is a dual-key dict, with both string and 
        #! integer keys. The integer keys range from 0 to N (N being the
        #! highest-level register), and represent the register address.
        #! String keys describe the register name.
        #!
        #! The values of this dict shall be a RegisterDescriptor 
        #! class instance.
        #! 
        #! @note To avoid duplicates for the register name, a prefix 
        #!  shall be appended to the name as needed.
        #!  E.G. ChannelConfigA to D will be prefixed with "CHxx", where
        #!  xx stands for the channel number.
        #!  Example: CH01ChannelConfigA, CH12ChannelConfigD
        #! 
        self.register_map = {}

        self.register_file = register_file

    def _add_register_descriptor(self, *args):
        new_register = RegisterDescriptor(*args)

        self.register_map[new_register.address] = new_register
        self.register_map[new_register.name] = new_register

    def __getitem__(self, reg_key)
        """
        """
        return self.register_map[reg_key]