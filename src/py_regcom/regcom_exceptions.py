
class RegcomCommunicationError(Exception):
    """Error to be raised on a general communication error with the RegCom device
    """
