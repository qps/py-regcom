

#! pylint: disable=unused-argument
class HWRegisterInterface:
    """Base class to provide a low-level interface for RegisterMap devices

    Details:
        This class serves as virtual base class with a predefined set of interfaces,
        to interact with, read, and modify the basic RegisterMap registers.

        It is to be used as base class for higher level adapters such as UART, SPI, or
        the DQAME services, to provide a unified interface.

        No translation features to take a register map CSV/Excel sheet will be provided,
        this shall be the job of a higher-level class.
    """

    def __init__(self):

        #! Register map cache. Dict with integer addresses of the register as key,
        #! the value being a uint32_t type value of the low-level data.
        self.register_data = {}
    
    def is_connected(self):
        """Returns whether or not an active connection with the RegisterMap device is present
        """
        return False
    

    def write_register(self, reg_no, reg_data):
        """Write data to a register.

        Details:
            This function will write the uint32_t data reg_data to the given register
            at address reg_no.
        """
        raise NotImplementedError()
    
    def read_register(self, reg_no):
        """Read a single register at given address
        """
        return self.read_registers([reg_no])[0]
    def read_registers(self, reg_list):
        """Read an array of register addresses.

        Details:
            This function shall return the data for an array of addresses.
            The purpose of this is that many protocols are more efficient when
            performing reads of batches of data, rather than individual reads, 
            and as such this is the preferred method of retrieving register data.

            The return value is an array of uint32_t, of the same length and order 
            as the input list of registers.
        """
        raise NotImplementedError()

    def send_command(self, command):
        """Send a RegisterMap command

        Details:
            Commands are commonly sent to a specific register value, and consist 
            themselves of a uint32_t value. This function is a wrapper around this
            register write call.
        """
        raise NotImplementedError()

    def send_reset(self):
        """Sends the command to reset the device
        """
        #self.send_command() #TODO Figure out reset command
    
    def send_save_settings(self):
        """Sends the command to save settings to flash
        """
        #self.send_command() #TODO Figure out save to flash command

