
import numpy as np

class Register(RegisterDescriptor):
    def __init__(self, interface, *args):
        super().__init__(*args);

        self.interface = interface

        self.current_raw_value = np.uint32(0)
        self.current_value = np.float(0)

        self.needs_write = False

    def write_float(self, value):
        pass

    def write_raw(self, value):
        pass
    
    def read(self):
        pass
        
class RegMapInterface(RegMapParser):
    def __init__(self, device_handler, *args):
        super().__init__(*args);
    
    def flush_writes(self):
        pass

    def _add_register_descriptor(self, *args):
        new_register = Register(self, *args)

        self.register_map[new_register.address] = new_register
        self.register_map[new_register.name] = new_register
    
    def read_register(self, register):
        affected_register = self.register_map[register]

        if(register == None):
            raise RuntimeError(f"Couldn't find register with key {register}!")
        
        # TODO fill out the rest of the read structure here